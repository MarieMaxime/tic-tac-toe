
abstract class Observer {

    constructor() {

    }

    onNotify(message: number): void {
        console.log(message);
    }
}

abstract class Subject {
    observers: Observer[] = [];
    constructor() {
    }

    addObserver(observer: Observer) {
        this.observers.push(observer);
    }

    notify(message: number) {
        for (let i = 0; i < this.observers.length; i++) {
            const element = this.observers[i];
            element.onNotify(message);
        }
    }
}

class Case extends Subject {

    grid!: HTMLElement;
    div!: HTMLElement;
    num!: number;
    player = 10;
    id!: number;
    constructor(id: number) {
        super();
        this.id = id;
        this.create();
    }

    create() {
        this.num = this.id;
        this.div = document.createElement("div");
        this.div.style.setProperty("width", "100px");
        this.div.style.setProperty("height", "100px");
        this.div.style.setProperty("background-color", "blue");
        this.div.style.setProperty("margin", "5px");
        this.div.style.setProperty("display", "inline-block");
        document.body.appendChild(this.div)
        if ((this.id + 1) % 3 == 0) {
            document.body.appendChild(document.createElement("br"));
          //  this.div.style.setProperty("display", "inline-block");
        }
       
        this.div.addEventListener("click", this.trigger.bind(this));
    }

    setColor(player: number) {
        if (player == 1) {
            this.div.style.setProperty("background-color", "green");
        } else if (player == 2) {
            this.div.style.setProperty("background-color", "red");
        }
    }

    setPlayer(player: number) {
        this.player = player;
    }

    getPlayer() {
        return this.player;
    }

    trigger(): void {
        //console.log('notify', this.num);
        this.notify(this.id);
    }


}


class GameController extends Observer {

    subject: Case[] = [];
    actualPlayer = 1;

    constructor() {
        super();


    }

    addSubject(subject: Case) {

        subject.addObserver(this);
        this.subject.push(subject);
        
    }

    onNotify(id: number): void {
     //   console.log('GameController say ', id);
       // console.log(this.subject[id]);
         let cases = this.subject[id];
         
         if (cases.getPlayer() == 10) {
             cases.setPlayer(this.actualPlayer);
             cases.setColor(this.actualPlayer);
             this.verifyWin();
             this.switchPlayer();
         }
    }

    verifyWin( ) {
        let winner = 0; 
        if ( this.subject[0].getPlayer() + this.subject[1].getPlayer() + this.subject[2].getPlayer() == 3 ) {
            winner = 1
        } else if ( this.subject[0].getPlayer() + this.subject[1].getPlayer() + this.subject[2].getPlayer() == 6 ) {
            winner = 2
        }

        if ( this.subject[3].getPlayer() + this.subject[4].getPlayer() + this.subject[5].getPlayer() == 3 ) {
            winner = 1
        } else if ( this.subject[3].getPlayer() + this.subject[4].getPlayer() + this.subject[5].getPlayer() == 6 ) {
            winner = 2
        }

        if ( this.subject[6].getPlayer() + this.subject[7].getPlayer() + this.subject[8].getPlayer() == 3 ) {
            winner = 1
        } else if ( this.subject[6].getPlayer() + this.subject[7].getPlayer() + this.subject[8].getPlayer() == 6 ) {
            winner = 2
        }

        if ( this.subject[0].getPlayer() + this.subject[3].getPlayer() + this.subject[6].getPlayer() == 3 ) {
            winner = 1
        } else if ( this.subject[0].getPlayer() + this.subject[3].getPlayer() + this.subject[6].getPlayer() == 6 ) {
            winner = 2
        }

        if ( this.subject[1].getPlayer() + this.subject[4].getPlayer() + this.subject[7].getPlayer() == 3 ) {
            winner = 1
        } else if ( this.subject[1].getPlayer() + this.subject[4].getPlayer() + this.subject[7].getPlayer() == 6 ) {
            winner = 2
        }

        if ( this.subject[2].getPlayer() + this.subject[5].getPlayer() + this.subject[8].getPlayer() == 3 ) {
            winner = 1
        } else if ( this.subject[2].getPlayer() + this.subject[5].getPlayer() + this.subject[8].getPlayer() == 6 ) {
            winner = 2
        }


        if ( this.subject[0].getPlayer() + this.subject[4].getPlayer() + this.subject[8].getPlayer() == 3 ) {
            winner = 1
        } else if ( this.subject[0].getPlayer() + this.subject[4].getPlayer() + this.subject[8].getPlayer() == 6 ) {
            winner = 2
        }

        if ( this.subject[2].getPlayer() + this.subject[4].getPlayer() + this.subject[6].getPlayer() == 3 ) {
            winner = 1
        } else if ( this.subject[2].getPlayer() + this.subject[4].getPlayer() + this.subject[6].getPlayer() == 6 ) {
            winner = 2
        }

        if (winner != 0) {
            let message = "Le joueur "+winner+" a gagné.";

            let div = document.createElement("div");
            div.innerHTML = message;

            document.body.appendChild(div);
        }
    
    }

    switchPlayer() {
        // if (this.actualPlayer i== 1) { this.actualPlayer = 2; }
        // else { this.player = 1; }

        this.actualPlayer == 1 ? this.actualPlayer = 2 : this.actualPlayer = 1
    }
}

class Render {
    constructor() {
        this.loadGrid()
    }

    loadGrid() {
        let controller = new GameController();
        for (let i = 0; i < 9; i++) {
            let cases = new Case(i);
            
            controller.addSubject(cases);
        }
    }

}

let render = new Render()